#ifndef DESKTOPCONFIG_H
#define DESKTOPCONFIG_H

#include <QWidget>

#include "global.h"

namespace Ui {
class DesktopConfig;
}

class DesktopConfig : public QWidget
{
    Q_OBJECT

public:
    explicit DesktopConfig(QWidget *parent = nullptr);
    ~DesktopConfig();

    void initUi();

    void saveConfig();

    void loadConfig();

private slots:
    void on_pb_exec_clicked();

    void on_pb_softDir_clicked();

    void on_pb_icon_clicked();

private:
    Ui::DesktopConfig *ui;
};

#endif // DESKTOPCONFIG_H
