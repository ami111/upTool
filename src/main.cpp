#include "uospackagetool.h"
#include <DApplication>
#include <DWidget>
#include <DWidgetUtil>

#include "global.h"

DWIDGET_USE_NAMESPACE

int main(int argc, char *argv[])
{
    DApplication a(argc, argv);
    a.loadTranslator();

    a.setOrganizationName("UOS");
    a.setApplicationName("upTool");
    a.setApplicationVersion("0.1");
    a.setApplicationDescription("deb打包工具");
    a.setProductIcon(QIcon("://upTool.png"));

    Global::g_execPath = QCoreApplication::applicationDirPath();

    UosPackageTool w;
    w.setMaximumSize(900,750);
    w.setMinimumSize(900,750);
    w.show();

    Dtk::Widget::moveToCenter(&w);
    return a.exec();
}
