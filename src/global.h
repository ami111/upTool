#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>
#include <QMap>

#include "packageinfo.h"

class Global
{
public:
    Global();

    static QString g_execPath;

    static PackageInfo *g_packageInfo;

    static QMap<QString, QString> *g_categoryMap;
};

#endif // GLOBAL_H
