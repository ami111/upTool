#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include <QWidget>

#include "global.h"

namespace Ui {
class HomePage;
}

class HomePage : public QWidget
{
    Q_OBJECT

public:
    explicit HomePage(QWidget *parent = nullptr);
    ~HomePage();

    void saveConfig();

    void loadConfig();
private slots:
    void on_pb_workDir_clicked();

    void on_le_softName_textChanged(const QString &arg1);

signals:
    void signalWorkDirExists(QString softDir);

private:
    Ui::HomePage *ui;
};

#endif // HOMEPAGE_H
