#-------------------------------------------------
#
# Project created by QtCreator 2022-08-01T22:52:35
#
#-------------------------------------------------

QT       += core gui dtkwidget

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = upTool
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        uospackagetool.cpp \
    controlconfig.cpp \
    global.cpp \
    main.cpp \
    packageinfo.cpp \
    desktopconfig.cpp \
    homepage.cpp \
    debworkform.cpp \
    workthread.cpp

HEADERS += \
        uospackagetool.h \
    controlconfig.h \
    global.h \
    packageinfo.h \
    desktopconfig.h \
    homepage.h \
    debworkform.h \
    workthread.h


FORMS += \
        uospackagetool.ui \
    controlconfig.ui \
    desktopconfig.ui \
    homepage.ui \
    debworkform.ui

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/apps/com.gitee.uptool/bin
#!isEmpty(target.path): INSTALLS += target
unix {
    isEmpty(PREFIX) {
        PREFIX = /opt/apps/com.gitee.uptool
    }

    BINDIR = $$PREFIX/files/bin
    DATADIR = $$PREFIX/entries

    target.path = $$BINDIR

    icon.files = upTool.png
    icon.path = $$DATADIR/icons

    desktop.files = upTool.desktop
    desktop.path = $$DATADIR/applications/

#    appdata.files = com.github.Bleuzen.FFaudioConverter.appdata.xml
#    appdata.path = $$DATADIR/metainfo/


    INSTALLS += target icon desktop
}
SUBDIRS += \
    UosPackageTool.pro

RESOURCES += \
    icon.qrc

