#ifndef PACKAGEINFO_H
#define PACKAGEINFO_H

#include <QString>

class PackageInfo
{
public:
    PackageInfo();

    QString m_packageName;      //包名

    QString m_workdir;
    QString m_softName;
    QString m_softVersion;
    QString m_softArch;
    QString m_softHomepage;
    QString m_softDescribe;
    QString m_softDepends;

    QString m_desktopExec;
    QString m_desktopIcon;
    QString m_desktopName;
    QString m_desktopCategory;
    QString m_softDir;

    QString m_mainterName;
    QString m_mainterEmail;

    QString m_debDebianPath;
    QString m_debFilePath;
    QString m_debApplicationPath;
    QString m_debIconPath;
};


#endif // PACKAGEINFO_H
