#ifndef DEBWORKFORM_H
#define DEBWORKFORM_H

#include <QWidget>

namespace Ui {
class DebWorkForm;
}

class DebWorkForm : public QWidget
{
    Q_OBJECT

public:
    explicit DebWorkForm(QWidget *parent = nullptr);
    ~DebWorkForm();

    // 设置textedit
    void setWorkProgress(QString str);
private:
    Ui::DebWorkForm *ui;
};

#endif // DEBWORKFORM_H
