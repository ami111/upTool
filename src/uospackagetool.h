#ifndef UOSPACKAGETOOL_H
#define UOSPACKAGETOOL_H

#include <DMainWindow>
#include <QStackedWidget>

#include "homepage.h"
#include "controlconfig.h"
#include "desktopconfig.h"
#include "debworkform.h"


DWIDGET_USE_NAMESPACE

namespace Ui {
class UosPackageTool;
}

class UosPackageTool : public DMainWindow
{
    Q_OBJECT

public:
    explicit UosPackageTool(QWidget *parent = nullptr);
    ~UosPackageTool();

    void initUi();

public slots:
    void slotWorkProgress(QString str);

protected slots:
    void slotWorkDirExists(QString softDir);

private slots:
    void on_pb_next_clicked();

    void on_pb_previous_clicked();

    void on_stackedWidget_currentChanged(int arg1);

    void on_pb_homepage_clicked();

    void on_pb_control_clicked();

    void on_pb_desktop_clicked();

    void on_pb_work_clicked();

private:
    Ui::UosPackageTool *ui;

    QStackedWidget *m_stackedWidget;

    HomePage *m_homePage;
    ControlConfig *m_controlConfig;
    DesktopConfig *m_desktopConfig;
    DebWorkForm *m_debWorkForm;
};

#endif // UOSPACKAGETOOL_H
