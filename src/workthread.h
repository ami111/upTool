#ifndef WORKTHREAD_H
#define WORKTHREAD_H

#include <QThread>
#include <QProcess>

class WorkThread : public QThread
{
    Q_OBJECT
public:
    WorkThread();

    // 创建工作目录
    void createWorkDir();
    // 保存配置为json文件
    void saveConfigToJson();

    // 创建打包目录(UOS规范)
    void createDebDir();

    // 创建control文件
    void createControlFile();

    // 创建desktop文件
    void createDesktopFile();

    // 创建info文件
    void createInfoFile();

    // 移动资源文件
    void moveSrcFile();

    // 打包
    void makeDebPackage();

    // 检测chrome-sandbox文件
    void checkSandboxFile();

    void startWork();

protected:
    void run();

signals:
    void signalWorkProgress(QString str);

protected slots:
    void readyReadStandardOutput();
    void readyReadStandardError();
    void finished();

private:
    QProcess *m_process;

    bool m_isComplete;
};

#endif // WORKTHREAD_H
