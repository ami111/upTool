#include "controlconfig.h"
#include "ui_controlconfig.h"

ControlConfig::ControlConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlConfig)
{
    ui->setupUi(this);
    initUi();
}

ControlConfig::~ControlConfig()
{
    delete ui;
}

void ControlConfig::initUi()
{
    ui->cb_arch->addItem("amd64");
    ui->cb_arch->addItem("arm64");
    ui->cb_arch->addItem("mips64el");
    ui->cb_arch->addItem("loogarch64");
    ui->cb_arch->addItem("all");
}

void ControlConfig::saveConfig()
{
    Global::g_packageInfo->m_packageName = ui->le_packageName->text();
    Global::g_packageInfo->m_softVersion = ui->le_version->text();
    Global::g_packageInfo->m_softArch = ui->cb_arch->currentText();
    Global::g_packageInfo->m_softHomepage = ui->le_homepage->text();
    Global::g_packageInfo->m_softDepends = ui->te_depends->toPlainText();
    Global::g_packageInfo->m_softDescribe = ui->te_describe->toPlainText();
}

void ControlConfig::loadConfig()
{
    ui->le_packageName->setText(Global::g_packageInfo->m_packageName);
    ui->le_version->setText(Global::g_packageInfo->m_softVersion);
    ui->cb_arch->setCurrentText(Global::g_packageInfo->m_softArch);
    ui->le_homepage->setText(Global::g_packageInfo->m_softHomepage);
    ui->te_depends->setText(Global::g_packageInfo->m_softDepends);
    ui->te_describe->setText(Global::g_packageInfo->m_softDescribe);
}
