#ifndef CONTROLCONFIG_H
#define CONTROLCONFIG_H

#include <QWidget>

#include "global.h"
#include "desktopconfig.h"

namespace Ui {
class ControlConfig;
}

class ControlConfig : public QWidget
{
    Q_OBJECT

public:
    explicit ControlConfig(QWidget *parent = nullptr);
    ~ControlConfig();

    void initUi();
    void saveConfig();

    void loadConfig();

private:
    Ui::ControlConfig *ui;

    DesktopConfig *m_desktopConfig;
};

#endif // CONTROLCONFIG_H
