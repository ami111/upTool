# upTool

## 介绍
upTool主要适用于uos系统上的一款二进制打包工具，如果有现成的可执行程序，但是没有打成deb包，可以通过该upTool进行简单配置，生成符合uos规范的deb包，可双击进行安装并在启动器展示。


## 编译使用
```  shell
sudo apt install git gcc qt5-default libdtkwidget-dev fakeroot
git clone https://gitee.com/uos-package/upTool.git
cd upTool/src
qmake
make
./upTool
```

## 打包安装
``` shell
sudo apt install devscripts build-essential git gcc qt5-default libdtkwidget-dev
mkdir uptool & cd uptool
git clone https://gitee.com/uos-package/upTool.git
cd upTool
debuild -b
```
## 使用示例
### 一，blender二进制打包示例
#### 1. blender下载
下载地址：https://www.blender.org/download/

下载完成后为blender-3.2.2-linux-x64.tar，解压该tar包后，确认二进制是否可直接执行。
#### 2.首页基本信息配置

<div align=center><img src="https://gitee.com/uos-package/uptool/raw/master/image/1.png"/> </div>

工作目录：打包信息存放的目录 (必填)

软件名称：需要打包的软件的名称，会与工作目录拼接成打包执行目录，生成deb包也存放于该目录。(必填)

作者：包维护者名称 (必填)

邮箱：包维护着邮箱 必填)

#### 3. control文件信息配置
<div align=center><img src="https://gitee.com/uos-package/uptool/raw/master/image/2.png"/> </div>

包名：安装包的名字，uos规范一般为倒置域名

版本：软件版本号

主页：软件主页，没有可写其他地址(github, gitee)

架构：可执行程序所对应的架构(需选择与主机同架构，全架构可选all)

依赖：软件运行所需要的依赖，按(cmake, g++, gcc)如下格式填写，没有可不填

描述：对软件的简单描述

#### 4. desktop文件信息配置
<div align=center><img src="https://gitee.com/uos-package/uptool/raw/master/image/3.png"/> </div>

主程序：可运行的二进制程序路径

图标： 该软件图标，用于启动器展示

目录：解压tar包后的总目录,会将该目录下文件拷贝到打包规定的目录

程序名： 用于启动器展示的名字

分类：用于启动器分类

#### 5. 打包进度展示
<div align=center><img src="https://gitee.com/uos-package/uptool/raw/master/image/4.png"/> </div>

如出现打包完成，可在首页配置的工作目录+软件名所在的目录找到安装包。

如出现打包失败，请检查所填写的内容。

### 二，motrix的appimage包转deb包示例
#### 1. motrix下载
下载地址：https://motrix.app/

切换到appimage包所在目录添加权限并解包：
``` shell
chmod +x Motrix-1.6.11.AppImage               
./Motrix-1.6.11.AppImage --appimage-extract    # appimage解包,
```
解压完成后目录为squashfs-root,后续步骤与上诉二进制打包相同

#### 2. chrome-sandbox文件
部分程序会存在chrome-sandbox文件，在uos上运行时需要添加root和4755权限。会弹出如下框进行认证,正确填写即可。
<div align=center><img src="https://gitee.com/uos-package/uptool/raw/master/image/pkexec.png"/> </div>

## 使用交流

QQ群：942418736
微信：18434754519 